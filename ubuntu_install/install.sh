#! /bin/bash
clear
apt-get update
apt-get upgrade -y
apt-get install -y python-dev
apt-get install -y python-setuptools
apt-get install -y python-pip
apt-get install -y libtiff5-dev 
apt-get install -y libjpeg8-dev 
apt-get install -y zlib1g-dev 
apt-get install -y libfreetype6-dev 
apt-get install -y liblcms2-dev 
apt-get install -y libwebp-dev 
apt-get install -y tcl8.6-dev 
apt-get install -y tk8.6-dev 
apt-get install -y python-tk
apt-get install -y python-opencv
apt-get install -y npm
apt-get install -y nodejs-legacy
apt-get install -y libpng-dev 
apt-get install -y libjpeg-dev 
apt-get install -y libtiff-dev
apt-get install -y gcc 
apt-get install -y g++
apt-get install -y autoconf 
apt-get install -y automake 
apt-get install -y libtool 
apt-get install -y checkinstall
apt-get install -y tesseract-ocr
npm install -g phantomjs  
ln /dev/null /dev/raw1394