from captchaSolver import CaptchaSolver
from collections import Counter
import itertools

products_folder = 'products'
try:
  
  digit_counter = {}
  
  for dilate, erode, blur, sharp, threshold in itertools.product(range(1,2), range(1,2), range(0,4), range(0,3), range(180,190,5)):
    
    cs = CaptchaSolver(products_folder+'/captcha.png').read_image()
    cs.dilate_image(dilate).erode_image(erode)
    cs.gaussian_blur_image(blur).bilateral_image().sharpen_image(sharp).noise_clean_image().threshold_image(threshold)
    cs.write_image(products_folder+'/modified.png')
    captcha = cs.process_image(products_folder+'/modified.png')
    
    print "dilate : %s, erode : %s, blur : %s, sharp : %s, threshold : %s, captcha : %s" % (dilate, erode, blur, sharp, threshold,captcha)
    
    digits = map(int,str(captcha))
    
    for i in range(0,len(digits)):
      if i not in digit_counter:
        digit_counter[i] = []
      digit_counter[i].append(digits[i])
  
  most_common1,num_most_common1 = Counter(digit_counter[0]).most_common(1)[0]
  most_common2,num_most_common2 = Counter(digit_counter[1]).most_common(1)[0]
  most_common3,num_most_common3 = Counter(digit_counter[2]).most_common(1)[0]
  most_common4,num_most_common4 = Counter(digit_counter[3]).most_common(1)[0]
  
  print str(str(most_common1)+str(most_common2)+str(most_common3)+str(most_common4))
except Exception as e:
  print e