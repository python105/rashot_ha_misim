#!/usr/bin/env python

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from datetime import datetime
import os
import json
import time
import sqlite3
import urlparse

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
  pass

#Create custom HTTPRequestHandler class
class RequestHandler(BaseHTTPRequestHandler):
  
  max_request = 25
  max_request_counter = 0

  def dict_factory(self,cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
  
  def do_HEAD(self):
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()
        
  #handle POST command
  def do_POST(self):
    
    try:
      
      length = int(self.headers.getheader('content-length'))
      field_data = self.rfile.read(length)
      fields = urlparse.parse_qs(field_data)
      if self.path == '/settlement':
        if len(fields) > 0 and 'settlement_name' in fields and len(fields['settlement_name'][0]) > 2:
          
          self.send_response(200)
          self.send_header('Content-type','application/json')
          self.send_header("Access-Control-Allow-Headers", "accept, content-type")
          self.send_header("Access-Control-Allow-Methods", "POST")
          self.send_header("Access-Control-Allow-Origin", "*")
          self.send_header("Connection", "close")
          self.end_headers()
          
          query_builder = 'SELECT settlement_name FROM settlement_list WHERE settlement_name LIKE ?'
          
          t=[]
          t.append(fields['settlement_name'][0].decode('utf8')+'%')
          conn = sqlite3.connect('dataStorage.db')
          conn.row_factory = self.dict_factory
          c = conn.cursor()
          c.execute(query_builder, t)
          result = c.fetchall()
          conn.close()
          self.wfile.write(json.dumps(result, sort_keys=True, indent=4))
      elif self.path == '/field':
        if len(fields) > 0 and (fields.keys()[0] == 'total_plot_size' or fields.keys()[0] == 'prop_type' or fields.keys()[0] == 'block_number'):
          
          self.send_response(200)
          self.send_header('Content-type','application/json')
          self.send_header("Access-Control-Allow-Headers", "accept, content-type")
          self.send_header("Access-Control-Allow-Methods", "POST")
          self.send_header("Access-Control-Allow-Origin", "*")
          self.send_header("Connection", "close")
          self.end_headers()
          
          query_builder = 'SELECT %s FROM transactions WHERE %s LIKE ? GROUP BY %s LIMIT 10' % (fields.keys()[0],fields.keys()[0],fields.keys()[0])
          
          t=[]
          t.append(fields.values()[0][0].decode('utf8')+'%')
          conn = sqlite3.connect('dataStorage.db')
          conn.row_factory = self.dict_factory
          c = conn.cursor()
          c.execute(query_builder, t)
          result = c.fetchall()
          conn.close()
          self.wfile.write(json.dumps(result, sort_keys=True, indent=4))
        else:
          self.send_error(404)
      elif self.path == '/query':
        RequestHandler.max_request_counter+=1
        print RequestHandler.max_request_counter
        if RequestHandler.max_request_counter > RequestHandler.max_request:
          self.send_response(200)
          self.send_header('Content-type','application/json')
          self.send_header("Access-Control-Allow-Headers", "accept, content-type")
          self.send_header("Access-Control-Allow-Methods", "POST")
          self.send_header("Access-Control-Allow-Origin", "*")
          self.send_header("Connection", "close")
          self.end_headers()
          self.wfile.write("Request Limit Exceeded")
        elif len(fields) > 0:
          
          self.send_response(200)
          self.send_header('Content-type','application/json')
          self.send_header("Access-Control-Allow-Headers", "accept, content-type")
          self.send_header("Access-Control-Allow-Methods", "POST")
          self.send_header("Access-Control-Allow-Origin", "*")
          self.end_headers()
          
          query_builder = 'SELECT * FROM transactions WHERE '
          query_builder+= 'total_plot_size=? AND '               if 'total_plot_size'  in fields else ''
          query_builder+= 'giving_money=? AND '                  if 'giving_money'     in fields else ''
          query_builder+= 'settlement_name LIKE ? AND '          if 'settlement_name'  in fields else ''
          query_builder+= 'prop_type=? AND '                     if 'prop_type'        in fields else ''
          query_builder+= 'block_number=? AND '                  if 'block_number'     in fields else ''
          query_builder+= 'sale_date=? AND '                     if 'sale_date'        in fields else ''
          query_builder+= 'giving_money <= ? AND '               if 'giving_money_bot' in fields else ''
          query_builder+= 'giving_money >= ? AND '               if 'giving_money_top' in fields else ''
          query_builder+= 'sale_date <= ? AND '                  if 'sale_date_bot'    in fields else ''
          query_builder+= 'sale_date >= ? AND '                  if 'sale_date_top'    in fields else ''
          query_builder = query_builder.rpartition('AND')[0]
          query_builder+= ' ORDER BY date(sale_date) DESC LIMIT 200'
          
          t=[]
          t.append(fields['total_plot_size'][0].decode('utf8'))  if 'total_plot_size'  in fields else ''
          t.append(fields['giving_money'][0].decode('utf8'))     if 'giving_money'     in fields else ''
          t.append(fields['settlement_name'][0].decode('utf8')+'%')  if 'settlement_name'  in fields else ''
          t.append(fields['prop_type'][0].decode('utf8'))        if 'prop_type'        in fields else ''
          t.append(fields['block_number'][0].decode('utf8'))     if 'block_number'     in fields else ''
          t.append(fields['sale_date'][0].decode('utf8'))        if 'sale_date'        in fields else ''
          t.append(fields['giving_money_bot'][0].decode('utf8')) if 'giving_money_bot' in fields else ''
          t.append(fields['giving_money_top'][0].decode('utf8')) if 'giving_money_top' in fields else '' 
          t.append(fields['sale_date_bot'][0].decode('utf8'))    if 'sale_date_bot'    in fields else '' 
          t.append(fields['sale_date_top'][0].decode('utf8'))    if 'sale_date_top'    in fields else '' 
          
          #t.append((datetime.strptime(fields['sale_date_top'][0].decode('utf8'),'%d/%m/%Y')).strftime('%Y-%m-%d')) if 'sale_date_top' in fields else '' 

          conn = sqlite3.connect('dataStorage.db')
          conn.row_factory = self.dict_factory
          c = conn.cursor()
          c.execute(query_builder, t)
          result = c.fetchall()
          conn.close()
          self.wfile.write(json.dumps(result, sort_keys=True, indent=4))
        else:
          self.send_error(404)
      else:
        self.send_error(404)
      
    except Exception as e:
      self.send_error(500,e)

  #handle GET command
  def do_GET(self):
    
    try:
      #Check the file extension required and set the right mime type
      if self.path.endswith(".html"):
        mimetype='text/html'
        sendReply = True
      elif self.path.endswith(".jpg"):
        mimetype='image/jpg'
        sendReply = True
      elif self.path.endswith(".gif"):
        mimetype='image/gif'
        sendReply = True
      elif self.path.endswith(".js"):
        mimetype='application/javascript'
        sendReply = True
      elif self.path.endswith(".css"):
        mimetype='text/css'
        sendReply = True
      else:
        sendReply = False

      if sendReply == True:
        #Open the static file requested and send it
        f = open(os.curdir + os.sep + self.path) 
        self.send_response(200)
        self.send_header('Content-type',mimetype)
        self.send_header("Connection", "close")
        self.end_headers()
        self.wfile.write(f.read())
        f.close()
      else:
        self.send_error(404)
        
    except Exception as e:
      self.send_error(500)

def run():
  #DataBase connection
  #print('sqlite3 server is starting...')
  #conn = sqlite3.connect('dataStorage.db')
  #c = conn.cursor()
  #print('sqlite3 server is running...')
  #HTTP server connection
  print('http server is starting...')
  server_address = ('', 3000)
  httpd = HTTPServer(server_address, RequestHandler,ThreadedHTTPServer)
  print('http server is running...')
  try:
    httpd.serve_forever()
  except KeyboardInterrupt:
    pass
  httpd.socket.close()
  httpd.server_close()
  print time.asctime(), "Server Stops - %s:%s" % (server_address[0], server_address[1])

if __name__ == '__main__':
  run()

