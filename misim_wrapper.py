from datetime import datetime, timedelta
import subprocess
import signal
import ctypes
import time
import sys
import os
import sqlite3
import pprint
import itertools
import argparse
import re

#python misim_wrapper.py --mode single --id 695 --offset 5 --enddate 30/12/2008
parser = argparse.ArgumentParser()
parser.add_argument('--mode', help='specify the run mode you want to use, ie:single or multiple',required=True)
parser.add_argument('--id', help='specify the id you want to use, ie:230',required=True)
parser.add_argument('--offset', help='specify the offset days you want, ie:10')
parser.add_argument('--startdate', help='specify the start date you want to search, ie:30/01/2017')
parser.add_argument('--enddate', help='specify the end date you want to search, ie:30/01/2017')
args = parser.parse_args()

id = args.id

if args.mode == "multiple":
  while True:
    commands = []

    conn = sqlite3.connect('dataStorage.db')
    c = conn.cursor()
    c.execute("SELECT * FROM proxy_list")
    proxy_list = c.fetchall()
    c.close()
    conn.close()

    if proxy_list:
      for single_proxy in proxy_list:
        pid,proxy,username,password = single_proxy
        if id > 2100:
          id = 1
        if sys.platform == 'win32':
          commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --executable wedrivers_win\phantomjs.exe --id %s --proxy %s --username %s --password %s " % (str(id),str(id),proxy,username,password))
          #commands.append("python www.misim.gov.il.py --folder products/%s --browser chrome --executable wedrivers_win\chromedriver.exe --id %s --proxy %s --username %s --password %s " % (str(id),str(id),proxy,username,password))
        else:
          commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --id %s --proxy %s --username %s --password %s" % (str(id),str(id),proxy,username,password))
        id = id + 1
    else:
      commands.append("python proxy_tester.py")
      
    try:
      processes = [subprocess.Popen(cmd, shell=True) for cmd in commands]
    except Exception as e:
      print "----------------error--------------------"
      print e
      
    for p in processes: p.wait()
    
elif args.mode == "single":
  commands = []
  
  conn = sqlite3.connect('dataStorage.db')
  c = conn.cursor()
  c.execute("SELECT proxy FROM proxy_list ORDER BY RANDOM() LIMIT 10")
  proxy_list = c.fetchall()
  c.close()
  conn.close()

  proxy_string = ' '.join(itertools.chain(*proxy_list))

  if sys.platform == 'win32':
    commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --executable wedrivers_win\phantomjs.exe --id %s --proxy %s --offset %s --enddate %s" % (str(id),str(id),proxy_string,args.offset,args.enddate))
  else:
    commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --id %s --proxy %s --offset %s --enddate %s" % (str(id),str(id),proxy_string,args.offset,args.enddate))
  
  try:
    processes = [subprocess.Popen(cmd, shell=True) for cmd in commands]
  except Exception as e:
    print "----------------error--------------------"
    print e
    
  for p in processes: p.wait()

# python misim_wrapper.py --id 1659,834,1910,1477,1865,1093,1449,378  --offset 1000 --mode mix
elif args.mode == "mix":
  while True:
    commands = []
    processes = []
    
    range_ids = re.findall("(\d+\-\d+)",id)
    for single_range in range_ids:
      range_search = re.search("(\d+)(\-)(\d+)",single_range)
      small_number = int((range_search).group(1))
      large_number = int((range_search).group(3))
      for i in xrange(small_number,large_number+1):
        id = id+","+str(i)
      id = re.sub(single_range+"," ,'', id)
    
    print id
    
    conn = sqlite3.connect('dataStorage.db')
    c = conn.cursor()
    c.execute("SELECT proxy FROM proxy_list ORDER BY RANDOM()")
    proxy_list = c.fetchall()
    c.execute("SELECT id,end_date FROM settlement_list WHERE id IN (%s)" % (id) )
    settlement_list = c.fetchall()
    c.close()
    conn.close()
    if len(proxy_list) < 5:
      proc = subprocess.Popen("python proxy_tester.py", shell=True)
      proc.wait()
      continue
    else:
      proxy_string = ' '.join(itertools.chain(*proxy_list))
      
      for key in range(0,len(settlement_list)):
        sid,end_date = settlement_list[key]
        if args.enddate is None:
          if end_date is not None:
            if int(datetime.strptime(end_date,'%d/%m/%Y').strftime('%Y')) <= 2000:
              continue
            enddate = end_date
          else:
            enddate = datetime.today().strftime('%d/%m/%Y')
        else:
          enddate = args.enddate

        if sys.platform == 'win32':
          commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --id %s  --executable wedrivers_win\phantomjs.exe --proxy %s --offset %s --enddate %s" % (sid,sid,proxy_string,args.offset,enddate))
        else:
          commands.append("python www.misim.gov.il.py --folder products/%s --browser phantomjs --id %s --proxy %s --offset %s --enddate %s" % (sid,sid,proxy_string,args.offset,enddate))
      
    try:
      for command in commands:
        process = subprocess.Popen(command, shell=True)
        processes.append((command,process))
    except Exception as e:
      print e
    
    while True:
      conn = sqlite3.connect('dataStorage.db')
      c = conn.cursor()
      c.execute("SELECT proxy FROM proxy_list")
      proxy_list = c.fetchall()
      c.close()
      conn.close()
      if len(proxy_list) < 5:
        for single in processes:
          command,process = single
          subprocess.Popen("TASKKILL /F /PID {pid} /T".format(pid=process.pid))
          process.wait()
        break
      print "----------------------------------------------------------"
      for key in range(len(processes)):
        command,process = processes[key]
        sid = (re.search( '(--id) (\d+)', command)).group(2)
        if process.poll() == 1:
          print "ID : %s STATE : %s" % (sid,"Stopped")
          subprocess.Popen("TASKKILL /F /PID {pid} /T".format(pid=process.pid))
          process.wait()
          process = subprocess.Popen(command, shell=True)
          processes[key] = (command,process)
        else:
          print "ID : %s STATE : %s" % (sid,"Running")
      print "----------------------------------------------------------"
      time.sleep(60)

else:
  print "No such mode"
  