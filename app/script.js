request_lock = false;

function search_transactions(event,form){
  event.preventDefault();
  var json_form = toJSONString(form);
  var data_form = toDATAString(form);
  
  if(request_lock === false){
    request_lock = true;
    setTimeout(function(){request_lock = false;},1000);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '../query', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
      try{
        if (JSON.parse(this.responseText).length > 0){
          drawColumnChart(JSON.parse(this.responseText),'columnChart');
          drawBubbleChart(JSON.parse(this.responseText),'bubbleChart');
          drawHistogramChart(JSON.parse(this.responseText),'histogramChart');
          drawScatterChart(JSON.parse(this.responseText),'scatterChart');
          drawPieChart(JSON.parse(this.responseText),'pieChart');
          drawTableChart(JSON.parse(this.responseText),'tableChart');
        }
      }catch(err){
        console.error(err);
      };
    };
    xhr.send(data_form);
  };
};

function search_field(field,request,stop_length){
    if(field.value.length >= stop_length && request_lock === false){
      request_lock = true;
      setTimeout(function(){request_lock = false;},1000);
      document.getElementById(field.getAttribute('list')).innerHTML = "";
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '../'+request, true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onload = function () {
        try{
          settlement_list = JSON.parse(this.responseText);
          for (index in settlement_list){
            var option_element = document.createElement('option');
            option_element.text = settlement_list[index][field.name];
            option_element.value = settlement_list[index][field.name];
            document.getElementById(field.getAttribute('list')).append(option_element);
          };
        }catch(err) {
          console.error(err);
        };
      };
      xhr.send(field.name+'='+field.value);
    };
  
};

function toJSONString(form){
  var obj = {};
  var elements = form.querySelectorAll("input, select, textarea");
  for(var i = 0; i < elements.length; ++i) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;
    if(name && value.length > 0){
      obj[name] = value;
    };
  };
  return JSON.stringify(obj);
};

function toDATAString(form){
  var urlEncodedDataPairs = [];
  var elements = form.querySelectorAll("input, select, textarea");
  for(var i = 0; i < elements.length; ++i) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;
    if(name && value.length > 0){
      urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(value));
    };
  };
  return urlEncodedDataPairs.join('&').replace(/%20/g, '+');
};

window.onload = function(){
	google.charts.load('current', {packages: ['corechart','table']});
}

function  drawColumnChart(dataArray,chartElementId) {
  var data = new google.visualization.DataTable();
  data.addColumn('date', 'תאריך');
  data.addColumn('number', 'סכום');
  data.addColumn({type:'string', role:'tooltip', 'p': {'html': true}});
  for (index in dataArray){
    var money = parseInt(dataArray[index].giving_money_details) || parseInt(dataArray[index].giving_money) || 0;
    data.addRow([
      new Date(dataArray[index].sale_date),
      money,
      '<div style="padding:10px">'+
        '<strong>מספר בלוק: </strong>'+dataArray[index].block_number+"</br>"+
        '<strong>סוג הנכס: </strong>'+dataArray[index].prop_type+"</br>"+
        '<strong>מספר חדרים: </strong>'+dataArray[index].number_rooms+"</br>"+
        '<strong>תאריך העיסקה: </strong>'+dataArray[index].sale_date+"</br>"+
        '<strong>שם יישוב: </strong>'+dataArray[index].settlement_name+"</br>"+
        '<strong>סכום העיסקה: </strong>'+money+"</br>"+
        '<strong>גודל השטח: </strong>'+dataArray[index].total_plot_size+"</br>"+
      '</div>'
    ]);
  };
  var options = {
    tooltip: { isHtml: true },
    bar: {groupWidth: "100%"},
    title: 'סכומי עסקאות נדלן',
    titlePosition: 'in',
    hAxis: {title: 'תאריך המכירה',format: 'dd/MM/yyyy',direction: -1},
    vAxis: {title: 'סכום העיסקה',format: '₪#,###',direction: 1},
    legend: {position: 'left'},
    reverseCategories: true,
    crosshair: { trigger: 'both', orientation: 'both' },
    series: {0:{ type: "bars", targetAxisIndex: 1 }},
    trendlines: { 0: {color: 'purple',lineWidth: 1,opacity: 0.8,type: 'polynomial',degree: 3,visibleInLegend: false},}
  };
  data.sort([{column: 0}]);
  var chart = new google.visualization.ColumnChart(document.getElementById(chartElementId));
  chart.draw(data, options);
};

function  drawScatterChart(dataArray,chartElementId) {
  var data = new google.visualization.DataTable();
  data.addColumn('date', 'תאריך');
  data.addColumn('number', 'סכום');
  data.addColumn({type:'string', role:'tooltip', 'p': {'html': true}});
  for (index in dataArray){
    var money = parseInt(dataArray[index].giving_money_details) || parseInt(dataArray[index].giving_money) || 0;
    data.addRow([
      new Date(dataArray[index].sale_date),
      money,
      '<div style="padding:10px">'+
        '<strong>מספר בלוק: </strong>'+dataArray[index].block_number+"</br>"+
        '<strong>סוג הנכס: </strong>'+dataArray[index].prop_type+"</br>"+
        '<strong>מספר חדרים: </strong>'+dataArray[index].number_rooms+"</br>"+
        '<strong>תאריך העיסקה: </strong>'+dataArray[index].sale_date+"</br>"+
        '<strong>שם יישוב: </strong>'+dataArray[index].settlement_name+"</br>"+
        '<strong>סכום העיסקה: </strong>'+money+"</br>"+
        '<strong>גודל השטח: </strong>'+dataArray[index].total_plot_size+"</br>"+
      '</div>'
    ]);
  };
  var options = {
    tooltip: { isHtml: true },
    bar: {groupWidth: "100%"},
    title: 'סכומי עסקאות נדלן',
    titlePosition: 'in',
    hAxis: {title: 'תאריך המכירה',format: 'dd/MM/yyyy',direction: -1},
    vAxis: {title: 'סכום העיסקה',format: '₪#,###',direction: 1},
    legend: {position: 'left'},
    reverseCategories: true,
    crosshair: { trigger: 'both', orientation: 'both' },
    series: {0:{  targetAxisIndex: 1 }},
    trendlines: { 0: {color: 'purple',lineWidth: 1,opacity: 0.8,type: 'polynomial',degree: 3,visibleInLegend: false},}
  };
  data.sort([{column: 0}]);
  var chart = new google.visualization.ScatterChart(document.getElementById(chartElementId));
  chart.draw(data, options);
};

function drawTableChart(dataArray,chartElementId) {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'שם יישוב');
  data.addColumn('string', 'שם האזור');
  data.addColumn('string', 'שם השכונה');
	data.addColumn('string', 'שם הרחוב');
	data.addColumn('number', 'מספר הבית');
	data.addColumn('number', 'מספר המגרש');
	data.addColumn('string', 'מספר גו"ח');
  data.addColumn('string', 'סוג העיסקה');
  data.addColumn('string', 'סוג הנכס');
  data.addColumn('string', 'ייעוד הנכס');
  data.addColumn('string', 'מטרת הנכס');
  data.addColumn('number', 'שנת בנייה');
  data.addColumn('number', 'גודל השטח ברוטו');
  data.addColumn('number', 'גודל השטח נטו');
  data.addColumn('number', 'גודל השטח הרשום');
  data.addColumn('number', 'מספר חדרים');
  data.addColumn('date', 'תאריך העיסקה');
  data.addColumn('number', 'סכום העיסקה');
  data.addColumn('number', 'שווי העיסקה המוערך');
  data.addColumn('number', 'סכום העיסקה מורחב');
	data.addColumn('number', 'שווי העיסקה המוערך מורחב');
  for (index in dataArray){
    data.addRow([
			dataArray[index].settlement_name,
			dataArray[index].area_name_details,
			dataArray[index].neighbourhood_name_details,
			dataArray[index].street_name_details,
			parseInt(dataArray[index].house_number_details) || 0,
			parseInt(dataArray[index].plot_number) || 0,
			dataArray[index].block_number,
			dataArray[index].deal_type_details,
			dataArray[index].prop_type,
			dataArray[index].prop_function_details,
			dataArray[index].prop_purpose_details,
			parseInt(dataArray[index].build_year) || 0,
			parseInt(dataArray[index].gross_plot_size_details) || 0,
			parseInt(dataArray[index].net_plot_size_details) || 0,
			parseInt(dataArray[index].register_plot_size_details) || 0,
			parseInt(dataArray[index].number_rooms) || 0,
      new Date(dataArray[index].sale_date),
      parseInt(dataArray[index].giving_money) || 0,
      parseInt(dataArray[index].value_money) || 0,
      parseInt(dataArray[index].giving_money_details) || 0,
      parseInt(dataArray[index].value_money_details) || 0
    ]);
  };
  var options = {
    showRowNumber: true,
		width: '100%'
  };
  data.sort([{column: 0}]);
  var chart = new google.visualization.Table(document.getElementById(chartElementId));
  chart.draw(data, options);
};

function drawBubbleChart(dataArray,chartElementId) {
  formatDataArray = [['שם יישוב','סכום העיסקה','תאריך העיסקה','סוג הנכס','גודל השטח']];
  for (index in dataArray){
    var money = parseInt(dataArray[index].giving_money_details) || parseInt(dataArray[index].giving_money) || 0;
    formatDataArray.push([
      dataArray[index].settlement_name,
      money ,
      new Date(dataArray[index].sale_date),
      dataArray[index].prop_type,
      parseInt(dataArray[index].total_plot_size) || parseInt(dataArray[index].register_plot_size) || 0
    ]);
  };
  data = google.visualization.arrayToDataTable(formatDataArray);
  var options = {
    title: 'סכומי עסקאות נדלן',
    titlePosition: 'in',
    vAxis: {title: 'תאריך המכירה',format: 'dd/MM/yyyy',direction: 1},
    hAxis: {title: 'סכום העיסקה',format: '₪#,###',direction: -1},
    legend: {position: 'right'},
    reverseCategories: true,
    crosshair: { trigger: 'both', orientation: 'both' },
    bubble: {textStyle: {fontSize: 11}}
  };
  var chart = new google.visualization.BubbleChart(document.getElementById(chartElementId));  
  chart.draw(data,options);
};

function drawHistogramChart(dataArray,chartElementId){
  formatDataArray = [['שם יישוב','סכום העיסקה']];
  for (index in dataArray){
    var money = parseInt(dataArray[index].giving_money_details) || parseInt(dataArray[index].giving_money) || 0;
    formatDataArray.push([
      dataArray[index].settlement_name,
      money
    ]);
  };
  data = google.visualization.arrayToDataTable(formatDataArray);
  var options = {
    title: 'כמות עיסקאות לפי פילוח סכומים',
    legend: { position: 'top', maxLines: 2 },
    colors: ['#5C3292', '#1A8763', '#871B47', '#999999'],
    interpolateNulls: false,
  };
  var chart = new google.visualization.Histogram(document.getElementById(chartElementId));
  chart.draw(data, options);
};

function drawPieChart(dataArray,chartElementId){
  formatDataArray = [['סוג נכס','כמות']];
	formatDataArray2 = [];
  for (index in dataArray){
		if (!formatDataArray[dataArray[index].prop_type]){
			formatDataArray[dataArray[index].prop_type] = [dataArray[index].prop_type,0];
		};
		formatDataArray[dataArray[index].prop_type][1] = formatDataArray[dataArray[index].prop_type][1]+1;
	};
	for (index in formatDataArray){
		formatDataArray2.push(formatDataArray[index]);
	};
  data = google.visualization.arrayToDataTable(formatDataArray2);
  var options = {
    title: 'כמות עיסקאות לפי פילוח סוג נכס',
    legend: { position: 'top', maxLines: 2 },
    colors: ['#5C3292', '#1A8763', '#871B47', '#1a0e9c', '#de9725', '#999999'],
    interpolateNulls: false,
  };
  var chart = new google.visualization.PieChart(document.getElementById(chartElementId));
  chart.draw(data, options);
};