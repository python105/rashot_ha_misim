#!/usr/bin/env python
# -*- coding: utf-8 -*-

#python www.misim.gov.il.py --folder products --browser phantomjs --id 200 --proxy 184.175.106.139:80 --debug 
#hping3 -S -p 80 201.16.147.193
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from collections import Counter

from utils import Utils
from captchaSolver import CaptchaSolver

from datetime import datetime, timedelta
from PIL import Image
import numpy as np
import cv2
import sqlite3
import hashlib
import StringIO
import base64
import sys
import os
import time
import requests
import traceback
import argparse
import random
import itertools


def search(property_type,deal_type,settlement,street,start_date,end_date):
    #Define Gloval variables
    global blockedCounter
    
    try:
      browser.get(search_page)
    except TimeoutException as e:
      return "nolink"
    
    main_search_form = utils.get_elements_by_css(browser,120,'body form')
    if main_search_form is None:
      return "nolink"
    
    browser.execute_script("window.alert = function() {};alert = function() {};")
    
    time.sleep(5)

    utils.input_present(browser,60,"txtYeshuv",settlement.decode('UTF-8')[:2],"settlement")
    
    settlement_obj = utils.get_element_by_xpath(browser,5,'//li[contains(text(), \''+settlement.decode("UTF-8")+'\') and @class="ui-menu-item"]')
    if settlement_obj is not None and hasattr(settlement_obj,'text') and settlement_obj.text:
      settlement_obj.click()

    utils.input_present(browser,10,"ctl00_ContentUsersPage_txtyomMechira_dateInput",start_date,"start date")

    utils.input_present(browser,10,"ctl00_ContentUsersPage_txtadYomMechira_dateInput",end_date,"end date")
    
    try:
      utils.select_present(browser,10,"ContentUsersPage_DDLTypeNehes",property_type.decode('UTF-8'),"property type")
      
      time.sleep(5)
      
      utils.select_present(browser,10,"ContentUsersPage_DDLMahutIska",deal_type.decode('UTF-8'),"deal type")
    except Exception as e:
      blockedCounter = blockedCounter + 1
      if blockedCounter < 5:
        return "BlockedSite"
      else:
        return "nolink"
      
    # get the captcha image
    img = utils.get_element_by_id(browser,120,"ContentUsersPage_RadCaptcha1_CaptchaImageUP")
    try:
      #loc, size = img.location_once_scrolled_into_view, img.size
      loc, size = img.location, img.size
      left, top = loc['x'], loc['y']
      
      width, height = size['width'], size['height']
      box = (int(left), int(top), int(left + width), int(top + height))

      screenshot = browser.get_screenshot_as_base64()
      img_screen = Image.open(StringIO.StringIO(base64.b64decode(screenshot)))

      crop_img = img_screen.crop(box)
      crop_img.save(products_folder+'/captcha.png', 'PNG')

      #Operations on the image
      
      digit_counter = {}
      
      for dilate, erode, blur, sharp, threshold in itertools.product(range(1,2), range(1,2), range(0,4), range(0,3), range(180,190,5)):
        
        cs = CaptchaSolver(products_folder+'/captcha.png').read_image()
        cs.dilate_image(dilate).erode_image(erode)
        cs.gaussian_blur_image(blur).bilateral_image().sharpen_image(sharp).noise_clean_image().threshold_image(threshold)
        cs.write_image(products_folder+'/modified.png')
        captcha = cs.process_image(products_folder+'/modified.png')
        
        digits = map(int,str(captcha))
        
        for i in range(0,len(digits)):
          if i not in digit_counter:
            digit_counter[i] = []
          digit_counter[i].append(digits[i])
      
      most_common1,num_most_common1 = Counter(digit_counter[0]).most_common(1)[0]
      most_common2,num_most_common2 = Counter(digit_counter[1]).most_common(1)[0]
      most_common3,num_most_common3 = Counter(digit_counter[2]).most_common(1)[0]
      most_common4,num_most_common4 = Counter(digit_counter[3]).most_common(1)[0]
      
      captcha = str(str(most_common1)+str(most_common2)+str(most_common3)+str(most_common4))
      
    except Exception as e:
      blockedCounter = blockedCounter + 1
      if blockedCounter < 5:
        return "BlockedSite"
      else:
        return "nolink"
      
    utils.input_present(browser,10,"ContentUsersPage_RadCaptcha1_CaptchaTextBox",captcha,"captcha")

    print "Tring captcha %s" % (captcha)
    
    utils.button_present(browser,10,"ContentUsersPage_btnHipus","Search")
    
    try:
      utils.element_present_boolean(browser,5,"ContentUsersPage_GridMultiD1")
    except Exception as e:
      utils.alert_present(browser,10)
      utils.alert_present(browser,10)
    
    if utils.element_present_boolean(browser,5,"ContentUsersPage_GridMultiD1") is not True:
      if utils.element_present_boolean(browser,5,"ContentUsersPage_lblerrDanger") is True:
        if utils.get_element_text_by_css(browser,1,"#ContentUsersPage_lblerrDanger") == "יש יותר מ-150 עסקאות נא למקד את החיפוש בעזרת חתך משני".decode('UTF-8'):
          return "TooMuchData"
        elif utils.get_element_text_by_css(browser,1,"#ContentUsersPage_lblerrDanger") == "לא נמצאו נתונים לחתך המבוקש".decode('UTF-8'):
          return "NoData"
      elif utils.get_element_text_by_css(browser,5,"#ContentUsersPage_LblJsErrorText") == "התאריך אינו בתחום הנתונים".decode('UTF-8'):
        return "LimitEnd"
      elif utils.get_element_text_by_css(browser,5,"#ContentUsersPage_LblJsErrorText") == "יש להזין ישוב בעברית בלבד".decode('UTF-8'):
        return "BadSettlementName"
      elif utils.get_element_text_by_css(browser,5,"#ContentUsersPage_LblJsErrorText") == "עליך להקליד אך ורק בעברית".decode('UTF-8'):
        return "NoHebrewType"
      elif utils.element_present_boolean(browser,10,"ContentUsersPage_RadCaptcha1_CaptchaImageUP") is not True:
        return "errorScreen"
      else:
        return "WrongCaptcha"
    else:
      return "Success"
      
def result(pageNumber):

  # Create table
  c.execute("CREATE TABLE IF NOT EXISTS transactions(id INTEGER PRIMARY KEY, hash_string varchar unique, block_number, sale_date, giving_money INTEGER, value_money INTEGER, prop_type, part_size, settlement_name, build_year, total_plot_size INTEGER, number_rooms INTEGER, area_name_details , block_number_details , neighbourhood_name_details , street_name_details , house_number_details INTEGER, register_plot_size_details INTEGER, prop_purpose_details , prop_function_details , house_function_details , plot_number_details INTEGER, gross_plot_size_details INTEGER, net_plot_size_details INTEGER, giving_money_details INTEGER, value_money_details INTEGER, build_year_details INTEGER, deal_type_details )")
  
  # Count total result pages
  totalPages = utils.get_elements_by_css(browser,10,'table tr.tabelPages table tr td')
  
  # Move to the currect pageNumber
  if totalPages is not None:
    page_element = utils.get_elements_by_css(totalPages[pageNumber],1,'a')
    if page_element is not None:
      try:
        page_element[0].click()
        time.sleep(5)
      except Exception as e:
        print "page button not available, error %s" % (e)
        browser.save_screenshot(products_folder+'/no_page_exception.png')
        return pageNumber
  else:
    print "page not available, error"
    browser.save_screenshot(products_folder+'/no_page_exception.png')
    return pageNumber

  # Get each result row
  table = utils.get_elements_by_css(browser,60,'table tr.row1,table tr.BoxB')
  for row in range(len(table)):
    
    #Reset all variables
    block_number = sale_date = giving_money = value_money = prop_type = part_size = settlement_name = build_year = total_plot_size = number_rooms = ''
    area_name_details = block_number_details = neighbourhood_name_details = street_name_details = ''
    house_number_details = register_plot_size_details = prop_purpose_details = prop_function_details = house_function_details = ''
    plot_number_details = gross_plot_size_details = net_plot_size_details = giving_money_details = ''
    value_money_details = build_year_details = deal_type_details = ''
    
    #Main Page list
    block_number = utils.get_element_text_by_css(table[row],1,'td:nth-child(2)')
    sale_date = utils.get_element_text_by_css(table[row],1,'td:nth-child(3)')
    giving_money = (utils.get_element_text_by_css(table[row],1,'td:nth-child(4)')).replace(',', '')
    value_money = (utils.get_element_text_by_css(table[row],1,'td:nth-child(5)')).replace(',', '')
    prop_type = utils.get_element_text_by_css(table[row],1,'td:nth-child(6)')
    part_size = utils.get_element_text_by_css(table[row],1,'td:nth-child(7)')
    settlement_name = utils.get_element_text_by_css(table[row],1,'td:nth-child(8)')
    build_year = utils.get_element_text_by_css(table[row],1,'td:nth-child(9)')
    total_plot_size = utils.get_element_text_by_css(table[row],1,'td:nth-child(10)')
    number_rooms = utils.get_element_text_by_css(table[row],1,'td:nth-child(11)')
    
    #Go in to inline details page
    details_button = utils.get_elements_by_css(table[row],1,'td:nth-child(2) a')
    if details_button is not None:
      try:
        details_button[0].click()

        area_name_details = utils.get_element_text_by_css(browser,5,'#ContentUsersPage_lblEzor')
        block_number_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblGush')
        neighbourhood_name_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblShchuna') 
        street_name_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblRechov')
        house_number_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblBayit')        
        register_plot_size_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblShetachRashum')
        prop_purpose_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblYeud')
        prop_function_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblTifkudBnyn')
        house_function_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblTifkudYchida')
        plot_number_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblMigrash')
        gross_plot_size_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblShetachNeto')
        net_plot_size_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblShetachNeto')
        giving_money_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblMcirMozhar').replace(',', '')
        value_money_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblMcirMorach').replace(',', '')
        build_year_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblShnatBniya')
        deal_type_details = utils.get_element_text_by_css(browser,1,'#ContentUsersPage_lblSugIska')
        
        utils.button_present(browser,1,'ContentUsersPage_btHazara','return button')
        utils.button_present(browser,1,'btHazara','return button for broken page')
        # Get each result row
        table = utils.get_elements_by_css(browser,60,'table tr.row1,table tr.BoxB')
      except Exception as e:
        print "details not available, error %s" % (e)
        browser.save_screenshot(products_folder+'/no_details_exception.png')
        return pageNumber

    #Change Sale_Date format to yyyy-mm-dd
    sale_date = (datetime.strptime(sale_date.decode('utf8'),'%d/%m/%Y')).strftime('%Y-%m-%d')
    pre_hash_string = block_number+sale_date+giving_money+prop_type+settlement_name
    hash_string = hashlib.md5(pre_hash_string.encode('utf-8')).hexdigest()

    # Insert a row of data
    try:
      c.execute(
      "INSERT INTO transactions VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      ( None , hash_string , block_number , sale_date , giving_money , value_money , prop_type , 
        part_size , settlement_name , build_year , total_plot_size , number_rooms ,
        area_name_details , block_number_details , neighbourhood_name_details , 
        street_name_details , house_number_details , register_plot_size_details , 
        prop_purpose_details , prop_function_details , house_function_details ,
        plot_number_details , gross_plot_size_details , net_plot_size_details , 
        giving_money_details , value_money_details , build_year_details , deal_type_details
      ))
      print "successfully add Hash"
    except sqlite3.IntegrityError:
      print "could not add Hash twice"
  
    # Save (commit) the changes
    conn.commit()
    
  if totalPages is not None and pageNumber<len(totalPages)-1:
    pageNumber+=1
    try:
      result(pageNumber)
    except Exception as e:
      print "next page not available , error : %s" % e
      return pageNumber
      
  return "Success"
        
def extract():
    letter_list = ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ל','מ','נ','ס','ע','פ','צ','ק','ר','ש','ת']
    try:
      browser.get(search_page)
    except TimeoutException as e:
      return "nolink"
    
    # Create table
    c.execute("CREATE TABLE IF NOT EXISTS settlement_list(id INTEGER PRIMARY KEY,settlement_name varchar unique)")
    
    for i in range(len(letter_list)):
      for i2 in range(len(letter_list)):
        utils.input_present(browser,10,"txtYeshuv",letter_list[i].decode('UTF-8')+letter_list[i2].decode('UTF-8'),"settlement")
        time.sleep(1)
        settlement_obj = utils.get_elements_by_css(browser,5,'ul#ui-id-1 li.ui-menu-item')
        if settlement_obj is not None:
          for row in range(len(settlement_obj)):
            if hasattr(settlement_obj[row],'text') and settlement_obj[row].text:
              # Insert a row of data
              try:
                c.execute("INSERT INTO settlement_list VALUES (?,?)",(None,settlement_obj[row].text))
                print "successfully add Settlement"
              except sqlite3.IntegrityError:
                print "could not add Settlement twice" 
      
        # Save (commit) the changes
        conn.commit()
         
def main(proxy_list,username,password,browser_type,browser_executable_path,products_folder_path,settlement_id,startdate,enddate,offset,debug):
  #extract()

  #Define Gloval variables
  global search_page
  global blockedCounter
  global utils
  global browser
  global conn
  global products_folder
  global c
  global end_date
  
  start_date = None
  search_result = None
  
  #Parameters
  search_page="https://www.misim.gov.il/svinfonadlan2010/startPageNadlan.aspx"
  blockedCounter = 0
  #Instances
  utils = Utils()

  #DataBase connection
  print('sqlite3 server is starting...')
  conn = sqlite3.connect('dataStorage.db')
  c = conn.cursor()
  print('sqlite3 server is running...')
  
  products_folder = products_folder_path

  if debug:
    utils.enablePrint()

  c.execute("SELECT id,settlement_name FROM settlement_list WHERE id = ?",(settlement_id,))
  settlement_result = c.fetchall()
  last_page = "Success"
  
  id,settlement_name = settlement_result[0]

  if offset:
    days_offset_initial = int(offset)
  else:
    days_offset_initial = 1000

  days_offset = days_offset_initial
    
  if enddate:
    dyn_end_date =  datetime.strptime(enddate,'%d/%m/%Y')
  else:
    dyn_end_date = datetime.today()
    
  if startdate:
    dyn_start_date = datetime.strptime(startdate,'%d/%m/%Y')
    reset_date =     datetime.strptime(startdate,'%d/%m/%Y')
  else:
    dyn_start_date = dyn_end_date-timedelta(days=days_offset)
    reset_date = datetime.strptime('1990','%Y')

  while reset_date.strftime('%Y') <= dyn_start_date.strftime('%Y'):
    
    if proxy_list:
      proxy = random.choice(proxy_list)
      print "Changing proxy to "+proxy
    
    if browser_type == 'phantomjs':
      #PhantomJS config
      dcap = dict(DesiredCapabilities.PHANTOMJS)
      dcap["phantomjs.page.settings.userAgent"] = (
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit"
      )
      service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
      if proxy is not None:
        service_args.append('--proxy=%s' % proxy)
        service_args.append('--proxy-type=https')
        if username is not None:
          service_args.append('--proxy-auth=%s:%s' % (username,password))
      if browser_executable_path:
        browser = webdriver.PhantomJS(executable_path=browser_executable_path,desired_capabilities=dcap,service_args=service_args)
      else:
        browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args)
    elif browser_type == 'chrome':
      #Chrome config
      prefs = {"profile.default_content_setting_values.notifications" : 2}
      chrome_options = webdriver.ChromeOptions()
      if proxy is not None:
        if username is not None:
          chrome_options.add_argument('--proxy-server=%s:%s@%s' % (username,password,proxy))
        else:
          chrome_options.add_argument('--proxy-server=%s' % proxy)
      chrome_options.add_experimental_option("prefs",prefs)
      if browser_executable_path:
        browser = webdriver.Chrome(executable_path=browser_executable_path,chrome_options=chrome_options)
      else:
        browser = webdriver.Chrome(chrome_options=chrome_options)
    elif browser_type == 'firefox':
      browser = webdriver.Firefox()
      
    #browser.implicitly_wait(60)
    browser.set_page_load_timeout(60)
    #browser.set_window_size(1024, 768)
    browser.maximize_window()
      
    property_type = "כללי"
    deal_type = "הכל"
    settlement = settlement_name.encode('UTF-8')
    street = ""
    if search_result != "WrongCaptcha" and days_offset == 0 and start_date == dyn_start_date.strftime('%d/%m/%Y'):
      dyn_start_date-=timedelta(days=1)
      dyn_end_date-=timedelta(days=1)
    start_date = dyn_start_date.strftime('%d/%m/%Y')
    end_date = dyn_end_date.strftime('%d/%m/%Y')
    now_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    print "%s : Fetching data between %s and %s for ID %s" % (now_time,start_date,end_date,settlement_id)
    
    c.execute("UPDATE settlement_list SET end_date = ? WHERE id = ? ",(end_date ,settlement_id,))
    conn.commit()
    
    search_result = search(property_type,deal_type,settlement,street,start_date,end_date)

    if search_result == "TooMuchData":
      print "*********Too much data found for dates*********"
      days_offset = days_offset/2
    elif search_result == "NoData":
      print "*********No data found for dates*********"
      days_offset = days_offset*2
    elif search_result == "errorScreen":
      print "*********Error screen present*********"
      dyn_start_date-=timedelta(days=1)
      dyn_end_date-=timedelta(days=1)
      continue
    elif search_result == "LimitEnd":
      print "*********Date limit was achieved*********"
      browser.quit()
      break
    elif search_result == "BadSettlementName":
      print "*********Wrong Settlement name*********"
      browser.quit()
      break
    elif search_result == "WrongCaptcha":
      print "*********Wrong Captcha*********"
      browser.save_screenshot(args.folder+'/wrong_captcha.png')
      browser.quit()
      continue
    elif search_result == "nolink":
      print "*********No Link to server*********"
      if proxy:
        try:
          c.execute("DELETE FROM proxy_list WHERE proxy = ?",[proxy])
          conn.commit()
          proxy_list.remove(proxy)
          print "successfully delete Proxy %s" % (proxy)
          browser.quit()
          continue
        except Exception as e:
          print e
          print "could not delete proxy %s" % (proxy) 
          browser.quit()
          break
      else:
        browser.quit()
        break
    elif search_result == "NoHebrewType":
      print "*********No Hebrew type*********"
      settlement_name = (settlement_name.encode('UTF-8')).replace('(', '')
    elif search_result == "BlockedSite":
      print "*********Site Was Blocked (%s)*********" % (blockedCounter)
      # Refresh the page in preventing of block message
      browser.delete_all_cookies()
      browser.refresh()
      browser.quit()
      continue
    elif search_result == "Success":
      print "*********Success*********"
      dyn_end_date = dyn_start_date
      
      if last_page is "Success":
        last_page = result(0)
        if last_page is not "Success":
          print "*********ReTrying page %s*********" % (last_page)
          # Refresh the page in preventing of block message
          browser.delete_all_cookies()
          browser.refresh()
          browser.quit()
          continue
      else:
        result(last_page)
        
    browser.quit()    
    dyn_start_date = dyn_end_date-timedelta(days=days_offset)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--proxy-list', help='specify the proxy-list you want to use, ie:184.175.106.139:80 164.165.10.139:8080',nargs='+')
  parser.add_argument('--username', help='specify the proxy username you want to use, ie:myuname')
  parser.add_argument('--password', help='specify the proxy password you want to use, ie:123456')
  parser.add_argument('--browser', help='specify the browser you want to use, ie:chrome/safari/phantomjs',default='phantomjs')
  parser.add_argument('--executable', help='specify the executable path you want to use, ie:wedrivers_win\phantomjs.exe')
  parser.add_argument('--folder', help='specify the products folder name path you want to use, ie:products',default='products')
  parser.add_argument('--id', help='specify the settlement id you want to use, ie:1001',required=True)
  parser.add_argument('--startdate', help='specify the start date you want to search, ie:30/01/2017')
  parser.add_argument('--enddate', help='specify the end date you want to search, ie:30/01/2017')
  parser.add_argument('--offset', help='specify the offset days you want to search, ie:100')
  parser.add_argument('--debug', help='specify debug on or off',action="store_true")
  args = parser.parse_args()
  #Products folder creation
  if not os.path.exists(args.folder):
    os.makedirs(args.folder)
  try:
    main(args.proxy_list,args.username,args.password,args.browser,args.executable,args.folder,args.id,args.startdate,args.enddate,args.offset,args.debug);
  except Exception as e:
    for frame in traceback.extract_tb(sys.exc_info()[2]):
      fname,lineno,fn,text = frame
      print 'EXCEPTION IN ({}, LINE {} "{}" "{}")'.format(fname, lineno, fn, text)
    print 'EXCEPTION : %s' % (e)
    
    c.close()
    conn.close()
    
    browser.save_screenshot(args.folder+'/exception.png')
    browser.quit()
    
    raise Exception('Sudden Stop!')
