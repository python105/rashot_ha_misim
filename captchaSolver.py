#-*- coding:utf-8 -*-
import cv2
import pytesseract
import numpy as np
import re
from PIL import Image
from PIL import ImageFilter

class CaptchaSolver(object):

  def __init__(self, image):
    self.image = image

  def read_image(self):
    self.image = cv2.imread(self.image)
    return self
  
  def write_image(self,path):
    cv2.imwrite(path, self.image)
    return self
    
  def process_image(self,img_path):
    image = Image.open(img_path)
    #pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
    result = pytesseract.image_to_string(image,config='digits -psm 8')
    return re.sub("\D","",result)
    
  def show_image(self,label):
    cv2.imshow(label,self.image)
    cv2.waitKey(0)
    return self
  
  def gray_image(self):
    self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
    return self
    
  def noise_clean_image(self):
    dst = cv2.fastNlMeansDenoisingColored(self.image,None,10,10,7,21)
    b,g,r = cv2.split(dst)
    self.image = cv2.merge([r,g,b])
    return self
    
  def gaussian_blur_image(self,iterations):
    for x in range(iterations):
      self.image = cv2.GaussianBlur(self.image, (5,5), 0)
    return self

  def median_blur_image(self,iterations):
    for x in range(iterations):
        self.image = cv2.medianBlur(self.image,5)
    return self
  
  def bilateral_image(self):
    self.image = cv2.bilateralFilter(self.image,13,70,50)
    return self

  def sharpen_image(self,iterations):
    kernel_sharpen = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    for x in range(iterations):
        self.image = cv2.filter2D(self.image, -1, kernel_sharpen)
    return self
    
  def subtract_images(self,img):
    sub_img = cv2.subtract(self.image, img)
    self.image = cv2.bitwise_not(sub_img)
    return self
    
  def morphology_image(self):
    kernel = np.ones((5,5),np.uint8)
    self.image = cv2.morphologyEx(self.image, cv2.MORPH_OPEN, kernel)
    return self
    
  def dilate_image(self, nbiter=0):
    kernel = np.ones((3,3),np.uint8)
    self.image = cv2.dilate(self.image,kernel,iterations = nbiter)
    return self
    
  def erode_image(self, nbiter=0):
    kernel = np.ones((3,3),np.uint8)
    self.image = cv2.erode(self.image,kernel,iterations = nbiter)
    return self
    
  def threshold_image(self,threshold):
    self.image = cv2.threshold(self.image,threshold,255, cv2.THRESH_BINARY)[1]
    return self