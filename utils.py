#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
import time
import os
import sys

class Utils:
    
  debug_switch = False
  
  def blockPrint(self):
      #sys.stdout = open(os.devnull, 'w')
      self.debug_switch = False

  def enablePrint(self):
      #sys.stdout = sys.__stdout__
      self.debug_switch = True
      
  def debugPrint(self,statement):
      if self.debug_switch == True:
        try:
          print statement
        except Exception as e:
          print e
        
  def element_present_boolean(self,driver,delay,elementId):
    result = False
    try:
      element_present = EC.presence_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementId )
      result = True
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementId )
    return result
    
  def get_element_by_id(self,driver,delay,elementId):
    result = None
    try:
      element_present = EC.presence_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementId )
      result = driver.find_element_by_id(elementId)
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementId )
    return result
  
  def get_elements_by_class(self,driver,delay,elementClass):
    result = None
    try:
      element_present = EC.presence_of_element_located((By.CLASS_NAME, elementClass))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementClass )
      result = driver.find_elements_by_class_name(elementClass)
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementClass )
    return result
    
  def get_elements_by_css(self,driver,delay,elementCss):
    result = None
    try:
      element_present = EC.presence_of_element_located((By.CSS_SELECTOR, elementCss))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementCss )
      result = driver.find_elements_by_css_selector(elementCss)
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementCss )
    return result
        
  def get_element_by_xpath(self,driver,delay,elementXpath):
    result = None
    try:
      element_present = EC.presence_of_element_located((By.XPATH, elementXpath))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementXpath )
      result = driver.find_element_by_xpath(elementXpath)
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementXpath )
    return result
    
  def get_element_text_by_css(self,driver,delay,elementCss):
    result = ''
    try:
      element_present = EC.presence_of_element_located((By.CSS_SELECTOR, elementCss))
      WebDriverWait(driver, delay).until(element_present)
      self.debugPrint( "Element %s Present" % elementCss )
      result = driver.find_elements_by_css_selector(elementCss)[0].text
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Element %s to load" % elementCss )
    return result
  
  def alert_present(self,driver,delay):
    try:
      WebDriverWait(driver,delay).until(EC.alert_is_present())
      self.debugPrint( "Alert Present" )
      alert = driver.switch_to_alert()
      alert.accept()
      self.debugPrint( "Alert Accepted" )
    except TimeoutException:
      self.debugPrint( "Timed out waiting for Alert to load" )
      
  def input_present(self,driver,delay,elementId,value,description):
    try:
      element_present = EC.presence_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      driver.find_element_by_id(elementId).clear()
      driver.find_element_by_id(elementId).send_keys(value)
      self.debugPrint( "Input %s found" % description )
    except TimeoutException:
        self.debugPrint( "Timed out waiting for input" )
        
  def select_present(self,driver,delay,elementId,value,description):
    try:
      element_present = EC.visibility_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      selected_element = Select(driver.find_element_by_id(elementId))
      time.sleep(1)
      selected_element.select_by_visible_text(value)
      self.debugPrint( "select %s found" % description )
    except TimeoutException:
        self.debugPrint( "Timed out waiting for select" )
  
  def select_present_by_css(self,driver,delay,elementCss,value,description):
    try:
      element_present = EC.visibility_of_element_located((By.CSS_SELECTOR, elementCss))
      WebDriverWait(driver, delay).until(element_present)
      selected_element = Select(driver.find_element_by_css_selector(elementCss))
      time.sleep(1)
      selected_element.select_by_visible_text(value)
      self.debugPrint( "select %s found" % description )
    except TimeoutException:
        self.debugPrint( "Timed out waiting for select" )

  def checkbox_present(self,driver,delay,elementId,value,description):
    try:
      element_present = EC.visibility_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      checkBox = driver.find_element_by_id(elementId)
      self.debugPrint( "select %s found" % description )
      if checkBox.is_selected() is value:
        self.debugPrint( "Checkbox was clicked" )
        checkBox.click()
    except TimeoutException:
        self.debugPrint( "Timed out waiting for checkbox" )
    
  def button_present(self,driver,delay,elementId,description):
    try:
      element_present = EC.visibility_of_element_located((By.ID, elementId))
      WebDriverWait(driver, delay).until(element_present)
      driver.find_element_by_id(elementId).click()
      self.debugPrint( "button %s found" % description )
    except TimeoutException:
        self.debugPrint( "Timed out waiting for button" )    

  def button_present_xpath(self,driver,delay,elementXpath,description):
    try:
      element_present = EC.visibility_of_element_located((By.XPATH, elementXpath))
      WebDriverWait(driver, delay).until(element_present)
      driver.find_element_by_xpath(elementXpath).click()
      self.debugPrint( "button %s found" % description )
    except TimeoutException:
        self.debugPrint( "Timed out waiting for button" )
  
  def execute_script(self,driver,script,argument):
    try:
      driver.execute_script(script, argument)
    except TimeoutException:
        self.debugPrint( "Timed out waiting for script" )
    