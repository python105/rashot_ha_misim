#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from PIL import Image
from utils import Utils
import urllib2, socket
import subprocess
import pprint
import time
import sys
import os
import sqlite3
import pytesseract
import cStringIO
import threading
from threading import Thread


def is_good_proxy(pip,port):    
    try:        
        proxy_handler = urllib2.ProxyHandler({'http': pip+':'+port})        
        opener = urllib2.build_opener(proxy_handler)
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        urllib2.install_opener(opener)        
        req=urllib2.Request('http://www.google.com')
        sock=urllib2.urlopen(req, timeout = 2)
    except urllib2.HTTPError, e:        
        return False
    except Exception, detail:
        return False
    return True
    
def get_demo_server_proxies():
  proxy_list = ["213.136.89.121:80","69.85.86.80:8080","217.119.82.14:8080",
                "177.67.84.135:8080","51.15.46.137:80","160.16.103.201:8080",
                "177.67.81.212:80","60.251.63.160:3128","106.185.24.64:8080",
                "103.238.215.194:8080","200.196.230.174:8080","161.148.1.94:80",
                "144.217.96.81:3128","158.69.220.80:80","177.67.82.102:8080"]
  return proxy_list
    
def get_torvpn_server_proxies():
  proxy_list = []
  dcap = dict(DesiredCapabilities.PHANTOMJS)
  dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
  service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
  
  if sys.platform == 'win32':
    browser = webdriver.PhantomJS(executable_path='wedrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
  else:
    browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  
  browser.set_page_load_timeout(60)
  browser.delete_all_cookies()
  browser.refresh()
  browser.get("https://www.torvpn.com/en/proxy-list")
  
  table = utils.get_elements_by_css(browser,60,'table.table-striped tbody tr')
  for row in range(len(table)):
    ip_addr   = utils.get_elements_by_css(table[row],10,'td:nth-child(2) img')
    port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(3)')
    country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(4) abbr')
    if ip_addr:
      img_src = ip_addr[0].get_attribute("src")
      file = cStringIO.StringIO(urllib2.urlopen(img_src).read())
      image = Image.open(file)
      ip_addr = pytesseract.image_to_string(image)
      if is_good_proxy(ip_addr,port_addr) is True:
        proxy_list.append(ip_addr+':'+port_addr) 
        print ip_addr+':'+port_addr
  
  browser.quit()
  
  return proxy_list

def get_sslproxies_server_proxies():
  proxy_list = []
  dcap = dict(DesiredCapabilities.PHANTOMJS)
  dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
  service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
  
  if sys.platform == 'win32':
    browser = webdriver.PhantomJS(executable_path='wedrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
  else:
    browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  

  browser.set_page_load_timeout(60)
  browser.delete_all_cookies()
  browser.refresh()
  browser.get("https://www.sslproxies.org")
  
  utils.select_present_by_css(browser,60,'div.dataTables_length select','80','change table length to 80')
  time.sleep(5)
  table = utils.get_elements_by_css(browser,60,'#proxylisttable tbody tr')
  for row in range(len(table)):
    ip_addr   = utils.get_element_text_by_css(table[row],10,'td:nth-child(1)')
    port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(2)')
    country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(3)')
    if is_good_proxy(ip_addr,port_addr) is True:
      proxy_list.append(ip_addr+':'+port_addr) 
      print ip_addr+':'+port_addr
  
  browser.quit()    
  
  return proxy_list
  
def get_hidemy_server_proxies():
  proxy_list = []
  dcap = dict(DesiredCapabilities.PHANTOMJS)
  dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
  service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
  
  if sys.platform == 'win32':
    browser = webdriver.PhantomJS(executable_path='wedrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
  else:
    browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  

  browser.set_page_load_timeout(60)
  browser.delete_all_cookies()
  browser.refresh()
  browser.get("https://hidemy.name/en/proxy-list/")
  
  time.sleep(5)
  pagin = utils.get_elements_by_css(browser,60,'div.proxy__pagination ul li')
  for page in range(len(pagin)-1):
    table = utils.get_elements_by_css(browser,60,'.proxy__t tbody tr')
    for row in range(len(table)):
      ip_addr   = utils.get_element_text_by_css(table[row],10,'td:nth-child(1)')
      port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(2)')
      country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(3)')
      if is_good_proxy(ip_addr,port_addr) is True:
        proxy_list.append(ip_addr+':'+port_addr) 
        print ip_addr+':'+port_addr
    try:
      next = utils.get_elements_by_css(browser,60,'div.proxy__pagination ul li.is-active + li a')
      next[0].click()
    except Exception as e:
      print e
  
  browser.quit()    
  
  return proxy_list
  
def get_proxylist_server_proxies():
  proxy_list = []
  dcap = dict(DesiredCapabilities.PHANTOMJS)
  dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
  service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
  
  if sys.platform == 'win32':
    browser = webdriver.PhantomJS(executable_path='wedrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
  else:
    browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  

  browser.set_page_load_timeout(60)
  browser.delete_all_cookies()
  browser.refresh()
  browser.get("https://proxy-list.org/english/index.php")
  
  time.sleep(5)
  pagin = utils.get_elements_by_css(browser,60,'div.table-menu a')
  for page in range(len(pagin)-1):
    table = utils.get_elements_by_css(browser,60,'div.table ul')
    for row in range(len(table)):
      proxy     = (utils.get_element_text_by_css(table[row],10,'li.proxy')).split(':')
      ip_addr   = proxy[0]
      port_addr = proxy[1]
      country   = utils.get_element_text_by_css(table[row],10,'li.country-city .country .name')
      if is_good_proxy(ip_addr,port_addr) is True:
        proxy_list.append(ip_addr+':'+port_addr) 
        print ip_addr+':'+port_addr
    try:    
      next = utils.get_elements_by_css(browser,60,'div.table-menu a.active + a')[0]
      next.click()
    except Exception as e:
      print e
  
  browser.quit()    
  
  return proxy_list
  
def check_proxy_vs_server(proxy):
 
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
    service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    service_args = [ '--proxy=%s' % proxy,'--proxy-type=https', '--ignore-ssl-errors=true','--ssl-protocol=any']
    
    if sys.platform == 'win32':
      browser = webdriver.PhantomJS(executable_path='wedrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
    else:
      browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  
   
    browser.set_page_load_timeout(80)
    socket.setdefaulttimeout(80)
    try:
      browser.get("https://www.misim.gov.il/svinfonadlan2010/startPageNadlan.aspx")
      utils.get_elements_by_css(browser,10,'body')
      if utils.element_present_boolean(browser,10,"ContentUsersPage_RadCaptcha1_CaptchaImageUP") is True:
        browser.quit()
        return True
      else:
        browser.quit()
        return False
    except Exception as e:
      browser.quit()
      return False

def insert_proxy_to_database(proxy):
  result = check_proxy_vs_server(proxy)
  if result is True:
    try:
      conn = sqlite3.connect('dataStorage.db')
      c = conn.cursor()
      c.execute("INSERT INTO proxy_list VALUES (?,?,?,?)",(None,proxy,None,None))
      conn.commit()
      c.close()
      conn.close()
      print "successfully add Proxy"
    except sqlite3.IntegrityError:
      print "could not add Proxy twice" 
  else:
    print "failed to add Proxy"
      
def check_server_proxy():
  
  proxy_list = get_sslproxies_server_proxies() + get_hidemy_server_proxies() + get_torvpn_server_proxies() + get_proxylist_server_proxies()
  #proxy_list = get_demo_server_proxies()
  
  threads = []
  for row in range(len(proxy_list)):
    try:
      print "Starting thread %s" % (row)
      t = Thread(target=insert_proxy_to_database, args=(proxy_list[row], ))
      t.start()
      threads.append(t)
      
    except Exception as e:
      print e
      print "Error: unable to start thread"
      
    if (row % 10 == 0 and row != 0) or (len(proxy_list)-1-row == 0):
      print "Joining threads"
      for t in threads:
        t.join()
      
    
    
        
def check_database_proxy():
  conn = sqlite3.connect('dataStorage.db')
  c = conn.cursor()
  c.execute("SELECT proxy FROM proxy_list")
  proxy_list = c.fetchall()
  for row in range(len(proxy_list)):
    result = check_proxy_vs_server(proxy_list[row])
    if result is False:
      try:
        c.execute("DELETE FROM proxy_list WHERE proxy = ?",(proxy_list[row]))
        conn.commit()
        print "successfully delete Proxy"
      except Exception as e:
        print "could not delete proxy" 
    else:
      print "%s is working" % proxy_list[row]
  c.close()
  conn.close()
    
    
utils = Utils()

conn = sqlite3.connect('dataStorage.db')
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS proxy_list(id INTEGER PRIMARY KEY,proxy varchar unique,username varchar,password varchar)")
c.close()
conn.close()
check_server_proxy()
#check_database_proxy()